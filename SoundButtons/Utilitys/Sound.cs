﻿using System;
using SQLite.Net.Attributes;

namespace SoundButtons
{
	public class Sound
	{
		[PrimaryKey,AutoIncrement]
		public int _id{get; set;}

		public string Label{ get; set; }
		public int Id{ get; set; }
		public string Type{ get; set; }



		public Sound(string l, int d,string t)
		{
			Label = l;
			Id = d;
			Type = t;

		}


	}
}

