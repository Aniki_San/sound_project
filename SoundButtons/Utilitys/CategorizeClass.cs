﻿using System;

namespace SoundButtons
{
	public class CategorizeClass
	{


		public static bool Anime(Sound vehicle)
		{
			return vehicle.Type== "animebutton";
		}

		public static bool Turk(Sound vehicle)
		{
			return vehicle.Type== "turkbutton";
		}

		public static bool Music(Sound vehicle)
		{
			return vehicle.Type== "musicbutton";
		}

		public static bool Game(Sound vehicle)
		{
			return vehicle.Type== "gamebutton";
		}

		public static bool Movie(Sound vehicle)
		{
			return vehicle.Type== "moviebutton";
		}
	}
}

