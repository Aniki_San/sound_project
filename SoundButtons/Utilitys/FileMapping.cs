﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SoundButtons
{
	public class FileMapping
	{
		

		public static List<Sound> GetFileNames()
		{
			List<Sound> FileStrings = new List<Sound>();




			int ID = 2130968576;
			FileStrings.Add (new Sound("Aferin Evlat ".ToUpper (), ID  , "turkbutton"));
			FileStrings.Add (new Sound("Ages of Turk ".ToUpper (), ID=ID+1  , "turkbutton"));
			FileStrings.Add (new Sound("Arrow in The Knee ".ToUpper (), ID=ID+1  , "gamebutton"));
			FileStrings.Add (new Sound("Aslan Pençesi ".ToUpper (), ID=ID+1  , "turkbutton"));
			FileStrings.Add (new Sound("Ave Maria ".ToUpper (), ID=ID+1  , "musicbutton"));
			FileStrings.Add (new Sound("Baka Yarroo ".ToUpper(),ID=ID+1,"animebutton"));
			FileStrings.Add (new Sound("Barbıe Girl ".ToUpper (), ID=ID+1  , "musicbutton"));
			FileStrings.Add (new Sound("Becaouse I Am Happy".ToUpper (), ID=ID+1  , "musicbutton"));
			FileStrings.Add (new Sound("Beyin Bedava".ToUpper (), ID=ID+1  , "turkbutton"));
			FileStrings.Add (new Sound("Bomb Has been Planted".ToUpper(),ID=ID+1,"gamebutton"));
			FileStrings.Add (new Sound("Breaveheart theme ".ToUpper(),ID=ID+1,"moviebutton"));
			FileStrings.Add (new Sound("Bune ford biz renocuyuz".ToUpper(),ID=ID+1,"turkbutton"));
			FileStrings.Add (new Sound("Bye Bye ".ToUpper(),ID=ID+1,"gamebutton"));
			FileStrings.Add (new Sound("Call Me Maybe ".ToUpper(),ID=ID+1,"musicbutton"));
			FileStrings.Add (new Sound("Cant Touch This ".ToUpper(),ID=ID+1,"musicbutton"));
			FileStrings.Add (new Sound("Celebration".ToUpper(),ID=ID+1,"musicbutton"));
			FileStrings.Add (new Sound("Çipetpet".ToUpper (), ID=ID+1  , "turkbutton"));
			FileStrings.Add (new Sound("Circus Song".ToUpper(),ID=ID+1,"musicbutton"));
			FileStrings.Add (new Sound("Combo Breaker".ToUpper(),ID=ID+1,"gamebutton"));
			FileStrings.Add (new Sound("Crash theme ".ToUpper(),ID=ID+1,"gamebutton"));
			FileStrings.Add (new Sound("DarkVader theme ".ToUpper(),ID=ID+1,"moviebutton"));
			FileStrings.Add (new Sound("Demacia".ToUpper(),ID=ID+1,"gamebutton"));
			FileStrings.Add (new Sound("Den Den MUshi".ToUpper(),ID=ID+1,"animebutton"));
			FileStrings.Add (new Sound("Dragon Balls Opening".ToUpper(),ID=ID+1,"animebutton"));
			FileStrings.Add (new Sound("Duck Hunt".ToUpper(),ID=ID+1,"gamebutton"));
			FileStrings.Add (new Sound("Ea Sports".ToUpper(),ID=ID+1,"gamebutton"));
			FileStrings.Add (new Sound("Epic Sux Guy ".ToUpper(),ID=ID+1,"musicbutton"));
			FileStrings.Add (new Sound("EveryBody Dance Now".ToUpper(),ID=ID+1,"musicbutton"));
			FileStrings.Add (new Sound("Fatality ".ToUpper(),ID=ID+1,"gamebutton"));
			FileStrings.Add (new Sound("Fight ".ToUpper(),ID=ID+1,"gamebutton"));
			FileStrings.Add (new Sound("Fıkrasına Gülünmeyen Adam".ToUpper (), ID=ID+1  , "musicbutton"));
			FileStrings.Add (new Sound("Freedom ".ToUpper(),ID=ID+1,"moviebutton"));
			FileStrings.Add (new Sound("Fus Roh Da ".ToUpper(),ID=ID+1,"gamebutton"));
			FileStrings.Add (new Sound("Game Over".ToUpper(),ID=ID+1,"gamebutton"));
			FileStrings.Add (new Sound("Gangnam Style".ToUpper(),ID=ID+1,"musicbutton"));
			FileStrings.Add (new Sound("Get in pozition".ToUpper(),ID=ID+1,"gamebutton"));
			FileStrings.Add (new Sound("Get Over Here!".ToUpper (), ID=ID+1  , "musicbutton"));
			FileStrings.Add (new Sound("Getsuga Tenshou".ToUpper(),ID=ID+1,"animebutton"));
			FileStrings.Add (new Sound("GhostBusters".ToUpper(),ID=ID+1,"musicbutton"));
			FileStrings.Add (new Sound("GodFather Theme".ToUpper(),ID=ID+1,"moviebutton"));
			FileStrings.Add (new Sound("Guile Theme ".ToUpper(),ID=ID+1,"gamebutton"));
			FileStrings.Add (new Sound("Hadouken ".ToUpper(),ID=ID+1,"gamebutton"));
			FileStrings.Add (new Sound("Hastala Vista Baby".ToUpper(),ID=ID+1,"moviebutton"));
			FileStrings.Add (new Sound("Headshot".ToUpper(),ID=ID+1,"gamebutton"));
			FileStrings.Add (new Sound("Heavy Machine Gun".ToUpper(),ID=ID+1,"gamebutton"));
			FileStrings.Add (new Sound("He Fu*ked Up".ToUpper(),ID=ID+1,"moviebutton"));
			FileStrings.Add (new Sound("Herkezin hayatına kimse karışamaz".ToUpper (), ID=ID+1  , "turkbutton"));
			FileStrings.Add (new Sound("Hobbits to Isengeard".ToUpper(),ID=ID+1,"moviebutton"));
			FileStrings.Add (new Sound("Hoşçakal Düşman Belde".ToUpper(),ID=ID+1,"turkbutton"));
			FileStrings.Add (new Sound("How about no".ToUpper(),ID=ID+1,"moviebutton"));
			FileStrings.Add (new Sound("How you doin".ToUpper(),ID=ID+1,"moviebutton"));
			FileStrings.Add (new Sound("Hunharca gülen adam".ToUpper(),ID=ID+1,"turkbutton"));
			FileStrings.Add (new Sound("I am Batman".ToUpper(),ID=ID+1,"moviebutton"));
			FileStrings.Add (new Sound ("I got Balls of steel".ToUpper (), ID = ID + 1, "gamebutton"));
			FileStrings.Add (new Sound("I like to move it".ToUpper(),ID=ID+1,"musicbutton"));
			FileStrings.Add (new Sound("Inspector gadged".ToUpper(),ID=ID+1,"moviebutton"));
			FileStrings.Add (new Sound("I will find you".ToUpper(),ID=ID+1,"moviebutton"));
			FileStrings.Add (new Sound("James Bond Theme".ToUpper(),ID=ID+1,"moviebutton"));
			FileStrings.Add (new Sound("John Cena".ToUpper(),ID=ID+1,"musicbutton"));
			FileStrings.Add (new Sound("Just Do it".ToUpper(),ID=ID+1,"moviebutton"));
			FileStrings.Add (new Sound("Kalinka".ToUpper(),ID=ID+1,"musicbutton"));
			FileStrings.Add (new Sound("Kamehamehaaaa".ToUpper(),ID=ID+1,"animebutton"));
			FileStrings.Add (new Sound("Kancık kelleni ödlek bedeninden".ToUpper(),ID=ID+1,"turkbutton"));
			FileStrings.Add (new Sound("Kill bill theme".ToUpper(),ID=ID+1,"moviebutton"));
			FileStrings.Add (new Sound("Leerooy ".ToUpper(),ID=ID+1,"gamebutton"));
			FileStrings.Add (new Sound("Lets Get Ready To Rumble".ToUpper(),ID=ID+1,"musicbutton"));
			FileStrings.Add (new Sound("Looney Tunes".ToUpper(),ID=ID+1,"moviebutton"));
			FileStrings.Add (new Sound("Lord theme ".ToUpper(),ID=ID+1,"moviebutton"));
			FileStrings.Add (new Sound("Luffy Song".ToUpper(),ID=ID+1,"animebutton"));
			FileStrings.Add (new Sound("Manha Manha".ToUpper(),ID=ID+1,"musicbutton"));
			FileStrings.Add (new Sound("Mario theme ".ToUpper(),ID=ID+1,"gamebutton"));
			FileStrings.Add (new Sound("Matrix".ToUpper(),ID=ID+1,"moviebutton"));
			FileStrings.Add (new Sound("Misty Mountions ".ToUpper(),ID=ID+1,"moviebutton"));
			FileStrings.Add (new Sound("My Precıous".ToUpper(),ID=ID+1,"moviebutton"));
			FileStrings.Add (new Sound("Name is Bond".ToUpper(),ID=ID+1,"moviebutton"));
			FileStrings.Add (new Sound("Nanana Batman".ToUpper(),ID=ID+1,"moviebutton"));
			FileStrings.Add (new Sound("Nınja turtles".ToUpper(),ID=ID+1,"moviebutton"));
			FileStrings.Add (new Sound("No god pls no".ToUpper(),ID=ID+1,"moviebutton"));
			FileStrings.Add (new Sound("NyanCat ".ToUpper(),ID=ID+1,"musicbutton"));
			FileStrings.Add (new Sound("Oh hell no".ToUpper(),ID=ID+1,"moviebutton"));
			FileStrings.Add (new Sound("Ölürsem Kabrime Gelme".ToUpper(),ID=ID+1,"turkbutton"));
			FileStrings.Add (new Sound("Önce Sen gebereceksin".ToUpper(),ID=ID+1,"turkbutton"));
			FileStrings.Add (new Sound("Over9000 ".ToUpper(),ID=ID+1,"animebutton"));
			FileStrings.Add (new Sound("Ow Ow ooooooh".ToUpper(),ID=ID+1,"musicbutton"));
			FileStrings.Add (new Sound("Pacman Dead".ToUpper(),ID=ID+1,"gamebutton"));
			FileStrings.Add (new Sound("Pacman Start".ToUpper(),ID=ID+1,"gamebutton"));
			FileStrings.Add (new Sound("Pink Phanter".ToUpper(),ID=ID+1,"moviebutton"));
			FileStrings.Add (new Sound("Pokemon opening".ToUpper(),ID=ID+1,"animebutton"));
			FileStrings.Add (new Sound("Race start".ToUpper(),ID=ID+1,"gamebutton"));
			FileStrings.Add (new Sound("Rickroll".ToUpper(),ID=ID+1,"musicbutton"));
			FileStrings.Add (new Sound("Rocky".ToUpper(),ID=ID+1,"moviebutton"));
			FileStrings.Add (new Sound("Ryu theme ".ToUpper(),ID=ID+1,"gamebutton"));
			FileStrings.Add (new Sound("SabreDance".ToUpper(),ID=ID+1,"musicbutton"));
			FileStrings.Add (new Sound("Sad Violin".ToUpper(),ID=ID+1,"musicbutton"));
			FileStrings.Add (new Sound("Sega".ToUpper(),ID=ID+1,"gamebutton"));
			FileStrings.Add (new Sound("Senersen ".ToUpper(),ID=ID+1,"turkbutton"));
			FileStrings.Add (new Sound("Sevmezdim".ToUpper(),ID=ID+1,"turkbutton"));
			FileStrings.Add (new Sound("Shotgun".ToUpper(),ID=ID+1,"gamebutton"));
			FileStrings.Add (new Sound("Sinra Tensei".ToUpper(),ID=ID+1,"animebutton"));
			FileStrings.Add (new Sound("SpongeBob".ToUpper(),ID=ID+1,"moviebutton"));
			FileStrings.Add (new Sound("Targed Acquıred".ToUpper(),ID=ID+1,"gamebutton"));
			FileStrings.Add (new Sound("Taylor Goat Edıtıon".ToUpper(),ID=ID+1,"musicbutton"));
			FileStrings.Add (new Sound("Tetris".ToUpper(),ID=ID+1,"gamebutton"));
			FileStrings.Add (new Sound("This is sparta".ToUpper(),ID=ID+1,"moviebutton"));
			FileStrings.Add (new Sound("Titanic Flute".ToUpper(),ID=ID+1,"musicbutton"));
			FileStrings.Add (new Sound("Transformers".ToUpper(),ID=ID+1,"moviebutton"));
			FileStrings.Add (new Sound("Trololo".ToUpper(),ID=ID+1,"musicbutton"));
			FileStrings.Add (new Sound("Tsubasa Opening".ToUpper(),ID=ID+1,"animebutton"));
			FileStrings.Add (new Sound("Tülay Geri Gell".ToUpper(),ID=ID+1,"turkbutton"));
			FileStrings.Add (new Sound("Wasted".ToUpper(),ID=ID+1,"gamebutton"));
			FileStrings.Add (new Sound("We will rock you".ToUpper(),ID=ID+1,"musicbutton"));
			FileStrings.Add (new Sound("What Are those".ToUpper(),ID=ID+1,"moviebutton"));
			FileStrings.Add (new Sound("What does the fox says".ToUpper(),ID=ID+1,"musicbutton"));
			FileStrings.Add (new Sound("Who are those".ToUpper(),ID=ID+1,"moviebutton"));
			FileStrings.Add (new Sound("Winter is coming".ToUpper(),ID=ID+1,"moviebutton"));
			FileStrings.Add (new Sound("Woody Laugh".ToUpper(),ID=ID+1,"moviebutton"));
			FileStrings.Add (new Sound("XFiles".ToUpper(),ID=ID+1,"moviebutton"));
			FileStrings.Add (new Sound("Yabadaduu".ToUpper(),ID=ID+1,"moviebutton"));
			FileStrings.Add (new Sound("Yagami Laught ".ToUpper(),ID=ID+1,"animebutton"));
			FileStrings.Add (new Sound("You Shall Not ".ToUpper(),ID=ID+1,"moviebutton"));
			FileStrings.Add (new Sound("You weak pathectic fool".ToUpper(),ID=ID+1,"gamebutton"));
			return FileStrings;
		}



	}
}

