﻿using System;

using Xamarin.Forms;
using System.Collections.Generic;

namespace SoundButtons
{
	public class MenuListData : List<MenuItem>
	{
		public MenuListData ()
		{
			this.Add(new MenuItem(){
				Title="All Sounds",
				IconSource="categories.png",
				TargetType=typeof(AllSounds)

			});

			this.Add(new MenuItem(){
				Title="Music",
				IconSource="music.png",
				TargetType=typeof(Music)

			});

			this.Add(new MenuItem(){
				Title="Game",
				IconSource="mario.jpg",
				TargetType=typeof(Games)

			});

			this.Add(new MenuItem(){
				Title="Movie",
				IconSource="movies.png",
				TargetType=typeof(Movies)

			});
					
			this.Add(new MenuItem(){
				Title="Anime",
				IconSource="anime.png",
				TargetType=typeof(Animes)

			});

			this.Add(new MenuItem(){
				Title="Turkey",
				IconSource="virals.png",
				TargetType=typeof(Turkey)

			});

//			this.Add(new MenuItem(){
//				Title="Favorites",
//				IconSource="virals.png",
//				TargetType=typeof(Favorites)
//
//			});
		}
	}
}


