﻿using System;

using Xamarin.Forms;

namespace SoundButtons
{
	public class MenuPage : ContentPage
	{

		public ListView Menu{ get; set; }

		public MenuPage ()
		{
			Title = "Categories";
			BackgroundColor = Color.FromHex ("333333");

			Menu = new MenuListView ();

			var menuLabel = new ContentView {
				Padding = new Thickness (5, 36, 5, 5),
				Content = new Label {
					TextColor = Color.FromHex ("AAAAAA"),
					Text = "Categories",
					FontSize=20

						
				}
			};

			var layout = new StackLayout {
				VerticalOptions = LayoutOptions.FillAndExpand
					
			};

			layout.Children.Add (menuLabel);

			layout.Children.Add (Menu);

			Content = layout;


		

		
		}
	}
}


