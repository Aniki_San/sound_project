﻿using System;
using SQLite.Net;

namespace SoundButtons
{
	public interface ISQLite
	{
		SQLiteConnection GetConnection();

	}
}


