﻿using System;
using SQLite.Net;
using Xamarin.Forms;
using System.Linq;
using System.Collections.Generic;


namespace SoundButtons
{
	public class DatabaseManager
	{
		List<Sound> SoundFiles=new List<Sound>();

		private static DatabaseManager instance;

		public static DatabaseManager Instance
		{
			get{
				if (instance == null) {
					instance = new DatabaseManager ();

				}

				return instance;
			}
		}

		SQLiteConnection database;



		public DatabaseManager()
		{

			SoundFiles=FileMapping.GetFileNames ();

			database = DependencyService.Get<ISQLite> ().GetConnection ();

			database.CreateTable<Sound> ();

		}


//		public IEnumerable<Sound> GetSounds() {
//			return (from t in database.Table<Sound> ()
//				select t).ToList ();
//		}
//
//		public Sound GetSound(int id) {
//			return database.Table<Sound> ().FirstOrDefault (t => t._id == id);
//		}
//

//
		public void AddSound(int sound,string lbl,string typ) {
			SoundFiles = FileMapping.GetFileNames ();

			
			Sound newSound = new Sound (lbl, sound, typ);
					
			database.Insert (newSound);
		}



//		public int AddSound(Sound sound)
//		{
//			
//			SoundFiles=FileMapping.GetFileNames ();
//
//
//
//			return sound._id == 0 ? database.Insert (sound) : database.Update (sound);
//
//		}
//
		public Sound GetSound(int id)
		{
			SoundFiles=FileMapping.GetFileNames ();

			return database.Get<Sound> (id);
		}


		public Sound[] GetSounds()
		{
			SoundFiles=FileMapping.GetFileNames ();

			return database.Table<Sound>().ToArray ();

		}


	}

}
