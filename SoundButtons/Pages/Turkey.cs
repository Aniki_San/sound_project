﻿using System;

using Xamarin.Forms;
using System.Collections.Generic;
using System.Linq;

namespace SoundButtons
{
	public class Turkey : ContentPage
	{

		public int  x;
		List<Sound> SoundFiles= new List<Sound>();
		//public static List<Sound> SoundFiles;
		Xamarin.Forms.ListView lvSounds;

		public void NormalGetFiles (){

			Device.BeginInvokeOnMainThread (() => {
				SoundFiles = FileMapping.GetFileNames();
				lvSounds.ItemsSource = null;
				lvSounds.ItemsSource = SoundFiles.Select (x=>{

					return new SoundFileHold {
						Id=x.Id,
						Label = x.Label,
						Type=x.Type,

					};
				}).ToArray ();

			});


		}


		public void FilterLocations (string filter)
		{
			if (string.IsNullOrWhiteSpace (filter)) {
				Device.BeginInvokeOnMainThread (() => {
					SoundFiles = FileMapping.GetFileNames ();
					SoundFiles.RemoveAll(CategorizeClass.Anime);
					SoundFiles.RemoveAll(CategorizeClass.Music);
					SoundFiles.RemoveAll(CategorizeClass.Movie);
					SoundFiles.RemoveAll(CategorizeClass.Game);
					lvSounds.ItemsSource = null;
					lvSounds.ItemsSource = SoundFiles.Select (x => {
						return new SoundFileHold {

							Id=x.Id,
							Label = x.Label,
							Type=x.Type

						};
					}).ToArray ();
				});
			} else
			{
				Device.BeginInvokeOnMainThread (() => {
					SoundFiles = FileMapping.GetFileNames ();
					SoundFiles.RemoveAll(CategorizeClass.Anime);
					SoundFiles.RemoveAll(CategorizeClass.Music);
					SoundFiles.RemoveAll(CategorizeClass.Movie);
					SoundFiles.RemoveAll(CategorizeClass.Game);
					lvSounds.ItemsSource = null;
					lvSounds.ItemsSource = SoundFiles.Select (x => {
						return new SoundFileHold {

							Id=x.Id,
							Label = x.Label,
							Type=x.Type

						};
					}).Where(a=> a.Label.ToLower().Contains(filter.ToLower())).ToArray ();
				});
			}


		}
		public Turkey ()
		{
			this.Title = "TURKEY !!";
			this.BackgroundImage = "background2.jpg";

			lvSounds = new Xamarin.Forms.ListView {

				HorizontalOptions = LayoutOptions.Center,
				RowHeight=100,


			};

			Image btnSearch = new Image {

				Source = "searchbutton.png",
				WidthRequest=50,
				HeightRequest=50,

			};

			Entry searchbr = new Entry
			{

				Placeholder = "Search Sound",
				WidthRequest=300


			};
			Button btnRandom = new Button {
				Image="random2.png",
				BackgroundColor=Color.Transparent,
				Text="RANDOM",
				FontSize=20,
				HeightRequest=100,
				FontFamily="Cochin-Bold",
			};

			NormalGetFiles ();

			int d = 0;

			for (int c = 0; c <= SoundFiles.Count-1; c++) {
				if (SoundFiles [c].Type == "turkbutton")
					d++;
			}

			int[] IDs = new int[d];

			int a = 0;

			int b = 0;

			for (int i = SoundFiles[0].Id; i <= SoundFiles[SoundFiles.Count-1].Id; i++){

				if (SoundFiles [a].Type == "turkbutton") {
					IDs [b] = i;
					b++;
				} 
				a++;
			}

			Random r = new Random ();

			btnRandom.Clicked += (sender, e) => {

				var index = r.Next (0, IDs.Length);
				x++;
				if(x==1){
					DependencyService.Get<IAudio> ().play (IDs[index]);

				}
				else if(x==2){

					DependencyService.Get<IAudio> ().stop(IDs[index]);
					x=0;

				}

				if(DependencyService.Get<IAudio> ().random()==false){
					btnRandom.Image="random2";

				}
				else if(DependencyService.Get<IAudio> ().random()==true)
				{
					btnRandom.Image="random";
				}


			};

			searchbr.TextChanged += (sender, e) => FilterLocations (searchbr.Text);


			lvSounds.ItemTemplate = new DataTemplate (typeof(SoundView));



			Device.BeginInvokeOnMainThread (() => {
				SoundFiles = FileMapping.GetFileNames();
				SoundFiles.RemoveAll(CategorizeClass.Anime);
				SoundFiles.RemoveAll(CategorizeClass.Music);
				SoundFiles.RemoveAll(CategorizeClass.Movie);
				SoundFiles.RemoveAll(CategorizeClass.Game);
				lvSounds.ItemsSource = null;
				lvSounds.ItemsSource = SoundFiles.Select (x => {
					return new SoundFileHold {

						Id=x.Id,
						Label = x.Label,
						Type=x.Type,

					};
				}).ToArray ();
			});

			lvSounds.ItemTapped += (sender, e) => {
				x++;
				var item = e.Item as SoundFileHold;
				//var SoundC = SoundFiles.FirstOrDefault(x => x.SoundFile == item.SoundFile);
				int SoundC=item.Id;

				if(x==1){
					DependencyService.Get<IAudio> ().play (SoundC);

				}
				else if(x==2){

					DependencyService.Get<IAudio> ().stop(SoundC);
					x=0;
				}

			};




			var searchBar = new StackLayout {
				Orientation=StackOrientation.Horizontal,
				Children = {
					btnSearch,
					searchbr,
				}

			};



			var MainContent = new StackLayout {
				Orientation=StackOrientation.Vertical,
				Spacing=20,
				Children = {
					searchBar,
					lvSounds,
					btnRandom
				}

			};

			this.Content = MainContent;

		}
			
	}
}


