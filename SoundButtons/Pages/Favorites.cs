﻿using System;

using Xamarin.Forms;
using System.Collections.Generic;
using System.Linq;

namespace SoundButtons
{
	public class Favorites : ContentPage
	{

		public int  x;
		List<Sound> SoundFiles= new List<Sound>();
		//public static List<Sound> SoundFiles;
		Xamarin.Forms.ListView lvSounds;
		public Favorites ()
		{
			this.BackgroundImage = "background2.jpg";


			lvSounds = new ListView {

				HorizontalOptions = LayoutOptions.Center,
				RowHeight=100,


			};

			Image btnSearch = new Image {

				Source = "searchbutton.png",
				WidthRequest=50,
				HeightRequest=50,

			};


			Entry searchbr = new Entry
			{

				Placeholder = "Search Sound",
				WidthRequest=300


			};


			Button btnRandom = new Button {
				Image="random2.png",
				BackgroundColor=Color.Transparent,
				Text="RANDOM",
				FontSize=20,
				HeightRequest=100,
				FontFamily="Cochin-Bold",
			};

			lvSounds.ItemTemplate = new DataTemplate (typeof(SoundView));

			Device.BeginInvokeOnMainThread (() => {
				SoundFiles = DatabaseManager.Instance.GetSounds ().ToList ();
				lvSounds.ItemsSource = null;
				lvSounds.ItemsSource = SoundFiles.Select (x => {
					return new SoundFileHold {

						Id=x.Id,
						Label = x.Label,
						Type=x.Type

					};
				}).ToArray ();
			});


			Content = new StackLayout { 

				VerticalOptions=LayoutOptions.FillAndExpand,
				Children = 
				{
					lvSounds
				}
				};

		}

		protected override void OnAppearing(){

			Device.BeginInvokeOnMainThread (() => {
				SoundFiles = DatabaseManager.Instance.GetSounds ().ToList ();
				lvSounds.ItemsSource = null;
				lvSounds.ItemsSource = SoundFiles.Select (x => {
					return new SoundFileHold {

						Id=x.Id,
						Label = x.Label,
						Type=x.Type

					};
				}).ToArray ();
			});


			base.OnAppearing ();


		}
	}
}


