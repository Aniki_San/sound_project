﻿using System;

using Xamarin.Forms;
using System.Collections.Generic;
using System.Linq;

namespace SoundButtons
{
	public class NovigationPop : ContentPage
	{
		int x=0;
		List<Sound> SoundFiles= new List<Sound>();

		public NovigationPop ()
		{

			Label lblHere = new Label {
				Text = "PRESS",
				FontAttributes = FontAttributes.Bold,
				FontFamily = "Thoma",
				FontSize = 40,
				TextColor = Color.White,
				XAlign=TextAlignment.Center,

			};

						this.BackgroundImage = "background.png";
			
						Label lblMusic= new Label {
							Text = "MUSIC",						
							FontSize = 20,
							TextColor = Color.Aqua,
							XAlign=TextAlignment.Center,
			
						};
			
						Button btnMusic = new Button {
			
							Image = "randomMusic",
							BorderRadius = 20,
							BorderWidth = 20,
							BackgroundColor=Color.Transparent,
							WidthRequest=100
			
			
						};
			
						StackLayout Music= new StackLayout {
							Orientation = StackOrientation.Vertical,
			
			
							Children = {
								
								lblMusic,
								btnMusic,
							}
			
						};
			
						Label lblMovie = new Label {
							Text = "MOVIE",
							FontSize = 20,
							TextColor = Color.FromRgb(0,102,204),
							XAlign=TextAlignment.Center,
			
			
						};
						Button btnMovie = new Button {
			
							Image = "randomMovie",
							BorderRadius = 20,
							BorderWidth = 20,
							BackgroundColor=Color.Transparent,
							WidthRequest=100
			
						};
			
						StackLayout Movie= new StackLayout {
							Orientation = StackOrientation.Vertical,
			
			
							Children = {
			
								lblMovie,
								btnMovie,
							}
			
						};
			
						Label lblGame = new Label {
							Text = "GAME",
							FontSize = 20,
							TextColor = Color.FromRgb(0,255,0),
							XAlign=TextAlignment.Center,
			
						};
						Button btnGame = new Button {
			
							Image = "randomGame",
							BorderRadius = 20,
							BorderWidth = 20,
							BackgroundColor=Color.Transparent,
							WidthRequest=100
			
			
						};
			
						StackLayout Game= new StackLayout {
							Orientation = StackOrientation.Vertical,
			
			
							Children = {
			
								lblGame,
								btnGame,
							}
			
						};
			
						Label lblTurk = new Label {
							Text = "TURK",
							FontSize = 20,
							XAlign=TextAlignment.Center,
							TextColor = Color.FromRgb(204,0,0)
			
						};
						Button btnTurk = new Button {
			
							Image = "randomTurk",
							BorderRadius = 20,
							BorderWidth = 20,
							BackgroundColor=Color.Transparent,
							WidthRequest=100
			
						};
			
						StackLayout Turk= new StackLayout {
							Orientation = StackOrientation.Vertical,
			
			
							Children = {
			
								lblTurk,
								btnTurk,
							}
			
						};
						Label lblAnime = new Label {
							Text = "ANIME",
							FontSize = 20,
							TextColor = Color.FromRgb(255,128,0),
							XAlign=TextAlignment.Center,
			
						};
						Button btnAnime = new Button {
			
							Image = "randomAnime",
							BorderRadius = 20,
							BorderWidth = 20,
							BackgroundColor=Color.Transparent,
							WidthRequest=100
			
						};
			
						StackLayout Anime= new StackLayout {
							Orientation = StackOrientation.Vertical,
			
							Children = {
			
								lblAnime,
								btnAnime,
							}
			
						};

			Button btnStart = new Button {

				Image = "start",


				BackgroundColor=Color.Transparent,

				HorizontalOptions=LayoutOptions.Center
			};

						SoundFiles = FileMapping.GetFileNames ();
			
						int musicInt = 0;
						int movieInt = 0;
						int gameInt = 0;
						int turkInt = 0;
						int animeInt = 0;
			
			
						for (int c = 0; c <= SoundFiles.Count-1; c++) {
							switch (SoundFiles[c].Type) {
							case "musicbutton":
								musicInt++;
								break;
							case "moviebutton":
								movieInt++;
								break;
							case "gamebutton":
								gameInt++;
								break;	
							case "turkbutton":
								turkInt++;
								break;	
							case "animebutton":
								animeInt++;
								break;
			
							default:
								break;
							}
			
			//				
						}
			
						int[] IDMusic = new int[musicInt];
						int[] IDMovie = new int[movieInt];
						int[] IDGame= new int[gameInt];
						int[] IDTurk = new int[turkInt];
						int[] IDAnime = new int[animeInt];
						int a = 0;
						int indexMusic = 0;
						int indexMovie = 0;
						int indexGame = 0;
						int indexTurk = 0;
						int indexAnime = 0;
			
						for (int i = SoundFiles[0].Id; i <= SoundFiles[SoundFiles.Count-1].Id; i++){
			
							switch (SoundFiles[a].Type) {
							case "musicbutton":
								IDMusic [indexMusic] = i;
								indexMusic++;
								break;
							case "moviebutton":
								IDMovie [indexMovie] = i;
								indexMovie++;
								break;
							case "gamebutton":
								IDGame [indexGame] = i;
								indexGame++;
								break;
							case "turkbutton":
								IDTurk [indexTurk] = i;
								indexTurk++;
								break;
							case "animebutton":
								IDAnime [indexAnime] = i;
								indexAnime++;
								break;
			
			
							default:
								break;
							}
							a++;
			
							
						}
			
						Random r = new Random ();
			
						btnMusic.Clicked += (sender, e) => {
							var index = r.Next (0, IDMusic.Length);
							x++;
							if(x==1){
								DependencyService.Get<IAudio> ().play (IDMusic[index]);
			
							}
							else if(x==2){
			
								DependencyService.Get<IAudio> ().stop(IDMusic[index]);
								x=0;
			
							}
			
							if(DependencyService.Get<IAudio> ().random()==false){
								btnMusic.Image="randomMusic";
			
							}
							else if(DependencyService.Get<IAudio> ().random()==true)
							{
								btnMusic.Image="randomMusicP";
							}
								
						};
			
						btnMovie.Clicked += (sender, e) => {
							var index = r.Next (0, IDMusic.Length);
							x++;
							if(x==1){
								DependencyService.Get<IAudio> ().play (IDMovie[index]);
			
							}
							else if(x==2){
			
								DependencyService.Get<IAudio> ().stop(IDMovie[index]);
								x=0;
			
							}
			
							if(DependencyService.Get<IAudio> ().random()==false){
								btnMovie.Image="randomMovie";
			
							}
							else if(DependencyService.Get<IAudio> ().random()==true)
							{
								btnMovie.Image="randomMovieP";
							}
			
						};
			
						btnGame.Clicked += (sender, e) => {
							var index = r.Next (0, IDGame.Length);
							x++;
							if(x==1){
								DependencyService.Get<IAudio> ().play (IDGame[index]);
			
							}
							else if(x==2){
			
								DependencyService.Get<IAudio> ().stop(IDGame[index]);
								x=0;
			
							}
			
							if(DependencyService.Get<IAudio> ().random()==false){
								btnGame.Image="randomGame";
			
							}
							else if(DependencyService.Get<IAudio> ().random()==true)
							{
								btnGame.Image="randomGameP";
							}
			
						};
			
						btnTurk.Clicked += (sender, e) => {
							var index = r.Next (0, IDTurk.Length);
							x++;
							if(x==1){
								DependencyService.Get<IAudio> ().play (IDTurk[index]);
			
							}
							else if(x==2){
			
								DependencyService.Get<IAudio> ().stop(IDTurk[index]);
								x=0;
			
							}
			
							if(DependencyService.Get<IAudio> ().random()==false){
								btnTurk.Image="randomTurk";
			
							}
							else if(DependencyService.Get<IAudio> ().random()==true)
							{
								btnTurk.Image="randomTurkP";
							}
			
						};
			
						btnAnime.Clicked += (sender, e) => {
							var index = r.Next (0, IDAnime.Length);
							x++;
							if(x==1){
								DependencyService.Get<IAudio> ().play (IDAnime[index]);
			
							}
							else if(x==2){
			
								DependencyService.Get<IAudio> ().stop(IDAnime[index]);
								x=0;
			
							}
			
							if(DependencyService.Get<IAudio> ().random()==false){
								btnAnime.Image="randomAnime";
			
							}
							else if(DependencyService.Get<IAudio> ().random()==true)
							{
								btnAnime.Image="randomAnimeP";
							}
			
						};






						SoundFiles=FileMapping.GetFileNames();

			
						int Startup = 0;
			
						int fight = 0;
			
			for (fight = 0; fight <= SoundFiles.Count-1; fight++) {
				if (SoundFiles [fight].Label.Contains ("Fight".ToUpper()))
					Startup = fight;
			
						}
			
			
			
			btnStart.Clicked += (sender, e) => {


				DependencyService.Get<IAudio> ().play(SoundFiles[Startup].Id);

				Navigation.PushModalAsync (new PopUp ());

			};



						StackLayout first = new StackLayout {
							Orientation = StackOrientation.Horizontal,
							Padding=new Thickness(10,50,0,0), 
							Spacing=15,
							Children = {
								Music,
								Movie,
								Turk,
			
							}
			
						};

			
						StackLayout second= new StackLayout {
							Orientation = StackOrientation.Horizontal,
			
							Padding=new Thickness(60,25,0,0), 
							Spacing=30,
							Children = {
								Game,
								Anime
							}
			
						};

			Content = new StackLayout {
				Orientation = StackOrientation.Vertical,
				Padding= new Thickness(0,20,0,0),			
				Children = {
					lblHere,
					first,
					second,
					btnStart
				}

			};
		}

	}
}

