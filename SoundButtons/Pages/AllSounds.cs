﻿using System;
using System.Linq;
using Xamarin.Forms;
using System.Collections.Generic;
using System.Threading.Tasks;


namespace SoundButtons
{
	
	public class AllSounds : ContentPage
	{
		

		public int  x;
		List<Sound> SoundFiles= new List<Sound>();
		//public static List<Sound> SoundFiles;
		ListView lvSounds;


		public void FilterLocations (string filter)
		{
			if (string.IsNullOrWhiteSpace (filter)) {
				Device.BeginInvokeOnMainThread (() => {
					SoundFiles = FileMapping.GetFileNames ();
					lvSounds.ItemsSource = null;
					lvSounds.ItemsSource = SoundFiles.Select (x => {
						
						return new SoundFileHold {
							
							Id=x.Id,
							Label = x.Label,
							Type=x.Type

						};
					}).ToArray ();
				});
			} else
			{
				Device.BeginInvokeOnMainThread (() => {
					SoundFiles = FileMapping.GetFileNames ();
					lvSounds.ItemsSource = null;
					lvSounds.ItemsSource = SoundFiles.Select (x => {
						return new SoundFileHold {

							Id=x.Id,
							Label = x.Label,
							Type=x.Type

						};
					}).Where(a=> a.Label.ToLower().Contains(filter.ToLower())).ToArray ();
				});
			}


		}

		public void NormalGetFiles (){

			Device.BeginInvokeOnMainThread (() => {
				SoundFiles = FileMapping.GetFileNames();
				lvSounds.ItemsSource = null;
				lvSounds.ItemsSource = SoundFiles.Select (x=>{

					return new SoundFileHold {
						Id=x.Id,
						Label = x.Label,
						Type=x.Type,

					};
				}).ToArray ();

			});


		}
	

		public AllSounds(){

			this.Title = "ALL SOUNDS";

			this.BackgroundImage = "background2.jpg";


			lvSounds = new ListView {
				
				HorizontalOptions = LayoutOptions.Center,
				RowHeight=90,


			};

			Image btnSearch = new Image {

				Source = "searchbutton.png",
				WidthRequest=50,
				HeightRequest=50,
					
			};


			Entry searchbr = new Entry
			{

				Placeholder = "Search Sound",
				WidthRequest=300


			};


			Button btnRandom = new Button {
				Image="random2.png",
				BackgroundColor=Color.Transparent,
				Text="RANDOM",
				FontSize=20,
				HeightRequest=100,
				FontFamily="Cochin-Bold",
			};








			searchbr.TextChanged += (sender, e) => FilterLocations (searchbr.Text);
				




			lvSounds.ItemTemplate = new DataTemplate (typeof(SoundView));


				Device.BeginInvokeOnMainThread (() => {
					SoundFiles = FileMapping.GetFileNames ();
					lvSounds.ItemsSource = null;
					lvSounds.ItemsSource = SoundFiles.Select (x => {
						return new SoundFileHold {
							
						Id=x.Id,
						Label = x.Label,
						Type=x.Type
							
						};
				}).ToArray ();
				});

			int[] IDs = new int[SoundFiles.Count];

			int a = 0;

			for (int i = SoundFiles[0].Id; i <= SoundFiles[SoundFiles.Count-1].Id; i++) {

				IDs [a] = i;
				a++;
			}



			Random r = new Random ();

			btnRandom.Clicked += (sender, e) => {

				var index = r.Next (0, IDs.Length);
				x++;


				if(x==1){
					DependencyService.Get<IAudio> ().play (IDs[index]);



				}
				else if(x==2){

					DependencyService.Get<IAudio> ().stop(IDs[index]);
					x=0;

				}

				if(DependencyService.Get<IAudio> ().random()==false){
				btnRandom.Image="random2";

				}
				else if(DependencyService.Get<IAudio> ().random()==true)
				{
					btnRandom.Image="random";
				}


			};


			lvSounds.ItemTapped += (sender, e) => {
				x++;
				var item = e.Item as SoundFileHold;
//				var item2=e.Item as SoundFileHold;

//				var SoundCurr = SoundFiles.FirstOrDefault(h => h.Id == item2.Id);
//
//				int soundID=item2.Id;
//
//				string soundLabel=item2.Label;
//
//				string soundType=item2.Type;
//
//				DatabaseManager.Instance.AddSound(soundID,soundLabel,soundType);
//
//				DatabaseManager.Instance.AddSound(SoundCurr);

				int SoundC=item.Id;

				if(x==1){
					
					DependencyService.Get<IAudio> ().play(SoundC);

				}
				else if(x==2){

					DependencyService.Get<IAudio> ().stop(SoundC);
					x=0;
				}

			
			};


		
			var searchBar = new StackLayout {
				Orientation=StackOrientation.Horizontal,
				Children = {
					btnSearch,
					searchbr,

				}

			};



			var MainContent = new StackLayout {
				Orientation=StackOrientation.Vertical,
				Spacing=20,
				Children = {
					searchBar,
					lvSounds,
					btnRandom
				}
					
			};

			this.Content = MainContent;





		}



}

}


