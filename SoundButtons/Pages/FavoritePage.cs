﻿using System;

using Xamarin.Forms;
using System.Collections.Generic;
using System.Linq;

namespace SoundButtons
{
	public class FavoritePage : ContentPage
	{
		public int  x;
		List<Sound> SoundFiles= new List<Sound>();
		//public static List<Sound> SoundFiles;
		Xamarin.Forms.ListView lvSounds;






		public FavoritePage ()
		{
			lvSounds = new ListView {

				HorizontalOptions = LayoutOptions.Center,
				RowHeight=100,


			};

			Button btnFavorite = new Button {
				WidthRequest=50,
				HeightRequest=50,

			};



			Image btnSearch = new Image {

				Source = "searchbutton.png",
				WidthRequest=50,
				HeightRequest=50,

			};

			Entry searchbr = new Entry
			{

				Placeholder = "Search Sound",
				WidthRequest=200


			};
					
			lvSounds.ItemTemplate = new DataTemplate (typeof(SoundView));


			Device.BeginInvokeOnMainThread (() => {
				SoundFiles = DatabaseManager.Instance.GetSounds().ToList();
				lvSounds.ItemsSource = null;
				lvSounds.ItemsSource = SoundFiles.Select (x => {
					return new SoundFileHold {

						Id=x.Id,
						Label = x.Label,
						Type=x.Type

					};
				}).ToArray ();
			});


			lvSounds.ItemTapped += (sender, e) => {
				x++;
				var item = e.Item as SoundFileHold;
				//var SoundC = SoundFiles.FirstOrDefault(x => x.SoundFile == item.SoundFile);
				int SoundC=item.Id;

				if(x==1){
					DependencyService.Get<IAudio> ().play (SoundC);

				}
				else if(x==2){

					DependencyService.Get<IAudio> ().stop(SoundC);
					x=0;
				}

			};




			var searchBar = new StackLayout {
				Orientation=StackOrientation.Horizontal,
				Children = {
					btnFavorite,
					btnSearch,
					searchbr,
				}

			};



			var MainContent = new StackLayout {
				Orientation=StackOrientation.Vertical,
				Spacing=20,
				Children = {
					searchBar,
					lvSounds
				}

			};

			this.Content = MainContent;

		}





		protected override void OnAppearing(){
			Device.BeginInvokeOnMainThread (() => {
				SoundFiles = DatabaseManager.Instance.GetSounds().ToList();
				lvSounds.ItemsSource = null;
				lvSounds.ItemsSource = SoundFiles.Select (x => {
					return new SoundFileHold {
						Id=x.Id,
						Label = x.Label,
						Type=x.Type
					};
				}).ToArray ();
			});


			base.OnAppearing ();
		}
	}
}


