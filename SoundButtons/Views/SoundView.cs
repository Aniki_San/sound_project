﻿using System;

using Xamarin.Forms;
using System.Collections.Generic;




namespace SoundButtons
{
	public class SoundView : ViewCell
	{
		public SoundView(){

			Image btnImage = new Image {

			};

			btnImage.SetBinding (Image.SourceProperty, "Type");



			var lblLabel= new Label{
				TextColor = Color.White,
				XAlign=TextAlignment.Center,
				YAlign=TextAlignment.Center,
				FontSize=15,
				FontFamily="Comic Sans MS",
			
			};

				
				lblLabel.SetBinding(Label.TextProperty, "Label");


		var stackRoot = new StackLayout {

				Orientation = StackOrientation.Horizontal,
				Spacing=20,
				Children = {
				btnImage,
				lblLabel,
				
				
			}

		};


			this.View = stackRoot;

	}
	}
	}



