﻿using System;

namespace SoundButtons
{
	public interface IAudio
	{
		void play(string fileName);

		void stop();
	}
}

