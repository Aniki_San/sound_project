﻿using System;
using Xamarin.Forms;
using SoundButtons.Droid;
using System.IO;
using SQLite.Net;

[assembly: Dependency(typeof(SqlLite_Android))]
namespace SoundButtons.Droid
{
	public class SqlLite_Android :ISQLite
	{
		public SqlLite_Android ()
		{
		}

		#region ISQLite implementation
		public SQLite.Net.SQLiteConnection GetConnection ()
		{

			string databaseFileName = "MovieSQLite.db3";

			string documentFolder = Environment.GetFolderPath (Environment.SpecialFolder.Personal);

			var platform = new SQLite.Net.Platform.XamarinAndroid.SQLitePlatformAndroid ();

			var path = Path.Combine (documentFolder, databaseFileName);

			var connection = new SQLiteConnection (platform, path);

			return connection;



		}
		#endregion
	}
}

