﻿using System;
using System.IO;
using SQLite.Net;
using Xamarin.Forms;
using SoundButtons.Droid;



[assembly: Dependency(typeof(SQLite_Android))]
namespace SoundButtons.Droid
{
	public class SQLite_Android
	{
		public SQLite_Android ()
		{
		}

		#region ISQLite implementation
		public SQLite.Net.SQLiteConnection GetConnection ()
		{

			string databaseFileName = "SoundSQLite.db3";

			string documentFolder = Environment.GetFolderPath (Environment.SpecialFolder.Personal);

			var path = Path.Combine (documentFolder, databaseFileName);

//			if (!File.Exists(path))
//			{
//				var s = Forms.Context.Resources.OpenRawResource(SQLite_Android);  // RESOURCE NAME ###
//
//				// create a write stream
//				FileStream writeStream = new FileStream(path, FileMode.OpenOrCreate, FileAccess.Write);
//				// write to the stream
//
//			}
//

			var platform = new SQLite.Net.Platform.XamarinAndroid.SQLitePlatformAndroid ();

			var connection = new SQLite.Net.SQLiteConnection (platform, path);

			return connection;



		}
		#endregion
	}
}

