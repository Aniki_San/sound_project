﻿using System;

using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using System.Collections.Generic;

namespace SoundButtons.Droid
{
	

	[Activity (Label = "SoundButtons.Droid", Theme="@android:style/Theme.Material",Icon = "@drawable/categories", MainLauncher = true, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation)]
	public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsApplicationActivity
	{



		protected override void OnCreate (Bundle bundle)
		{
			

			base.OnCreate (bundle);

			global::Xamarin.Forms.Forms.Init (this, bundle);



			LoadApplication (new App ());
		}
	}
}

